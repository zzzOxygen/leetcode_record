package SystemDesign;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * https://leetcode-cn.com/problems/design-twitter/
 */
public class LC355 {
    static class Twitter {

        private int time = 1;
        //followMap的value必须用Set,不能用List,因为会重复关注，例如A关注B,A关注B，会导致重复获取tweet
        private Map<Integer, Set<Integer>> followMap = new ConcurrentHashMap<>();
        private Map<Integer, List<int[]>> tweetMap = new ConcurrentHashMap<>();

        /**
         * Initialize your data structure here.
         */
        public Twitter() {

        }

        /**
         * Compose a new tweet.
         */
        public void postTweet(int userId, int tweetId) {
            if (!tweetMap.containsKey(userId)) {
                ArrayList<int[]> list = new ArrayList<>();
                list.add(new int[]{tweetId, time});
                tweetMap.put(userId, list);
            } else {
                tweetMap.get(userId).add(new int[]{tweetId, time});
            }
            time++;
        }

        /**
         * Retrieve the 10 most recent tweet ids in the user's news feed. Each item in the news feed must be posted
         * by users who the user followed or by the user herself. Tweets must be ordered from most recent to least
         * recent.
         */
        public List<Integer> getNewsFeed(int userId) {
            List<Integer> ret = new ArrayList<>();
            List<int[]> tweetList = new ArrayList<>();
            if (followMap.containsKey(userId)) {
                followMap.get(userId).forEach(integer -> {
                    if (tweetMap.containsKey(integer)) {
                        tweetList.addAll(tweetMap.get(integer));
                    }
                });
            }
            if (tweetMap.containsKey(userId)) {
                tweetList.addAll(tweetMap.get(userId));
            }
            tweetList.sort((o1, o2) -> o2[1] - o1[1]);
            for (int i = 0; i < 10; i++) {
                if (i < tweetList.size()) {
                    ret.add(tweetList.get(i)[0]);
                } else break;
            }
            return ret;
        }

        /**
         * Follower follows a followee. If the operation is invalid, it should be a no-op.
         */
        public void follow(int followerId, int followeeId) {
            Set<Integer> tmp;
            if (!followMap.containsKey(followerId)) {
                tmp = new HashSet<>();
            } else {
                tmp = followMap.get(followerId);
            }
            tmp.add(followeeId);
            followMap.put(followerId, tmp);
        }

        /**
         * Follower unfollows a followee. If the operation is invalid, it should be a no-op.
         */
        public void unfollow(int followerId, int followeeId) {
            followMap.remove(followerId);
        }
    }

    public static void main(String[] args) {
        Twitter twitter = new Twitter();
        twitter.follow(1, 5);
        System.out.println(twitter.getNewsFeed(1));
        twitter.postTweet(1, 5);
        System.out.println(twitter.getNewsFeed(1));
        twitter.follow(1, 2);
        twitter.postTweet(2, 6);
        System.out.println(twitter.getNewsFeed(1));
        twitter.unfollow(1, 2);
        System.out.println(twitter.getNewsFeed(1));
    }
}
